function parseMolecule(formula) {
  lexemes = decompose(formula);
  var result = {};
  var brOptions = {')': '(', ']': '[', '}': '{'};
  var brackets = [];
  var brCoeff = [];
  var inCoeff = 1;
  // Go through the string in reverse so that we see the bracket index _before_ the contents.
  for (var j = lexemes.length - 1; j >= 0; j--) {
    if (typeof(lexemes[j]) === 'number' && lexemes[j - 1][0] >= 'A' && lexemes[j - 1][0] <= 'Z') {
      // Remember the index if it corresponds to an atom.
      inCoeff = lexemes[j];
    } else if (typeof(lexemes[j]) === 'number') {
      // Indices after the brackets are pushed to an array.
      brCoeff.push(lexemes[j]);
    } else if (lexemes[j][0] >= 'A' && lexemes[j][0] <= 'Z') {
      // Add elements to the dictionary with the corresponding indices/products of nested bracket indices.
      result[lexemes[j]] ? result[lexemes[j]] += inCoeff*brCoeff.reduce((a,b) => a * b, 1) : result[lexemes[j]] = inCoeff*brCoeff.reduce((a,b) => a * b, 1);
      inCoeff = 1;
    } else if (lexemes[j] === brackets.slice(-1)[0]) {
      // Accept only an opening bracket that matches the current closing one.
      if (!(/[a-z\{\(\[]/gi).test(lexemes[j + 1])) {
        // Can't have a number or a closing bracket after an opening bracket.
        throw new Error();
      }
      brackets.pop(); 
      
      brCoeff.pop();
    } else {
      // Handle closing brackets.
      brackets.push(brOptions[lexemes[j]]);
      if (typeof(lexemes[j + 1]) !== 'number') {
        // Index == 1 can be omitted.
        brCoeff.push(1)
      }
    }
  }
  if (brackets.length > 0) {throw new Error()}
  return result;
}

// Helper function to convert the original formula string into an array of atoms, indices and brackets.
// For example: "Al2(SO4)3" -> ["Al", 2, "(", "S", "O", 4, ")", 3]
function decompose(formula) {
  var lexemes = [];
  for (var i = 0; i < formula.length; i++) {
    // Separate out individual atoms. Names can have more than one letter, but always start from a capital letter.
    if (formula[i] >= 'A' && formula[i] <= 'Z') {
      for (var j = i + 1; j < formula.length && formula[j] >= 'a' && formula[j] <= 'z'; j++)
        /* pass */;
      lexemes.push(formula.slice(i, j));
      i = j - 1;
    } else if (formula[i] >= '0' && formula[i] <= '9') {
      for (var j = i + 1; j < formula.length && formula[j] >= '0' && formula[j] <= '9'; j++)
        /* pass */;
      lexemes.push(parseInt(formula.slice(i, j)));
      i = j - 1;
    } else if ('[]{}()'.includes(formula[i])) {
      lexemes.push(formula[i]);
    } else {
      throw new Error();
    }
  }
  return lexemes;
}