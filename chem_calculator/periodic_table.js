periodicTable = {
  H: {
    name: 'Hydrogen',
    atomic_weight: 1.008
  },
  He: {
    name: 'Helium',
    atomic_weight: 4.003
  },
  Li: {
    name: 'Lithium',
    atomic_weight: 6.939
  },
  Be: {
    name: 'Beryllium',
    atomic_weight: 9.012
  },
  B: {
    name: 'Boron',
    atomic_weight: 10.811
  },
  C: {
    name: 'Carbon',
    atomic_weight: 12.011
  },
  N: {
    name: 'Nitrogen',
    atomic_weight: 14.007
  },
  O: {
    name: 'Oxygen',
    atomic_weight: 15.999
  },
  F: {
    name: 'Fluorine',
    atomic_weight: 18.998
  },
  Ne: {
    name: 'Neon',
    atomic_weight: 20.183
  },
  Na: {
    name: 'Sodium',
    atomic_weight: 22.99
  },
  Mg: {
    name: 'Magnesium',
    atomic_weight: 24.312
  },
  Al: {
    name: 'Aluminium',
    atomic_weight: 26.982
  },
  Si: {
    name: 'Silicon',
    atomic_weight: 28.086
  },
  P: {
    name: 'Phosphorus',
    atomic_weight: 30.974
  },
  S: {
    name: 'Sulphur',
    atomic_weight: 32.064
  },
  Cl: {
    name: 'Chlorine',
    atomic_weight: 35.453
  },
  Ar: {
    name: 'Argon',
    atomic_weight: 39.948
  },
  K: {
    name: 'Potassium',
    atomic_weight: 39.102
  },
  Ca: {
    name: 'Calcium',
    atomic_weight: 40.08
  },
  Sc: {
    name: 'Scandium',
    atomic_weight: 44.956
  },
  Ti: {
    name: 'Titanium',
    atomic_weight: 47.9
  },
  V: {
    name: 'Vanadium',
    atomic_weight: 50.94
  },
  Cr: {
    name: 'Chromium',
    atomic_weight: 52
  },
  Mn: {
    name: 'Manganese',
    atomic_weight: 54.94
  },
  Fe: {
    name: 'Iron',
    atomic_weight: 55.85
  },
  Co: {
    name: 'Cobalt',
    atomic_weight: 58.93
  },
  Ni: {
    name: 'Nickel',
    atomic_weight: 58.71
  },
  Cu: {
    name: 'Copper',
    atomic_weight: 63.54
  },
  Zn: {
    name: 'Zinc',
    atomic_weight: 65.37
  },
  Ga: {
    name: 'Gallium',
    atomic_weight: 69.72
  },
  Ge: {
    name: 'Germanium',
    atomic_weight: 72.59
  },
  As: {
    name: 'Arsenic',
    atomic_weight: 74.92
  },
  Se: {
    name: 'Selenium',
    atomic_weight: 78.96
  },
  Br: {
    name: 'Bromine',
    atomic_weight: 79.909
  },
  Kr: {
    name: 'Krypton',
    atomic_weight: 83.8
  },
  Rb: {
    name: 'Rubidium',
    atomic_weight: 85.47
  },
  Sr: {
    name: 'Strontium',
    atomic_weight: 87.62
  },
  Y: {
    name: 'Yttrium',
    atomic_weight: 88.905
  },
  Zr: {
    name: 'Zirconium',
    atomic_weight: 91.22
  },
  Nb: {
    name: 'Niobium',
    atomic_weight: 92.906
  },
  Mo: {
    name: 'Molybdenum',
    atomic_weight: 95.94
  },
  Tc: {
    name: 'Technetium',
    atomic_weight: 99
  },
  Ru: {
    name: 'Ruthenium',
    atomic_weight: 101.07
  },
  Rh: {
    name: 'Rhodium',
    atomic_weight: 102.92
  },
  Pd: {
    name: 'Palladium',
    atomic_weight: 106.4
  },
  Ag: {
    name: 'Silver',
    atomic_weight: 107.87
  },
  Cd: {
    name: 'Cadmium',
    atomic_weight: 112.4
  },
  In: {
    name: 'Indium',
    atomic_weight: 114.82
  },
  Sn: {
    name: 'Tin',
    atomic_weight: 118.69
  },
  Sb: {
    name: 'Antimony',
    atomic_weight: 121.75
  },
  Te: {
    name: 'Tellurium',
    atomic_weight: 127.6
  },
  I: {
    name: 'Iodine',
    atomic_weight: 126.904
  },
  Xe: {
    name: 'Xenon',
    atomic_weight: 131.3
  },
  Cs: {
    name: 'Caesium',
    atomic_weight: 132.905
  },
  Ba: {
    name: 'Barium',
    atomic_weight: 137.34
  },
  La: {
    name: 'Lanthanum',
    atomic_weight: 138.91
  },
  Ce: {
    name: 'Cerium',
    atomic_weight: 140.12
  },
  Pr: {
    name: 'Praseodymium',
    atomic_weight: 140.907
  },
  Nd: {
    name: 'Neodymium',
    atomic_weight: 144.24
  },
  Pm: {
    name: 'Promethium',
    atomic_weight: 147
  },
  Sm: {
    name: 'Samarium',
    atomic_weight: 150.35
  },
  Eu: {
    name: 'Europium',
    atomic_weight: 151.96
  },
  Gd: {
    name: 'Gadolinium',
    atomic_weight: 157.25
  },
  Tb: {
    name: 'Terbium',
    atomic_weight: 158.92
  },
  Dy: {
    name: 'Dysprosium',
    atomic_weight: 162.5
  },
  Ho: {
    name: 'Holmium',
    atomic_weight: 164.93
  },
  Er: {
    name: 'Erbium',
    atomic_weight: 167.26
  },
  Tm: {
    name: 'Thulium',
    atomic_weight: 168.93
  },
  Yb: {
    name: 'Ytterbium',
    atomic_weight: 173.04
  },
  Lu: {
    name: 'Lutetium',
    atomic_weight: 174.97
  },
  Hf: {
    name: 'Hafnium',
    atomic_weight: 178.49
  },
  Ta: {
    name: 'Tantalum',
    atomic_weight: 180.95
  },
  W: {
    name: 'Tungsten',
    atomic_weight: 183.85
  },
  Re: {
    name: 'Rhenium',
    atomic_weight: 186.2
  },
  Ir: {
    name: 'Osmium',
    atomic_weight: 192.2
  },
  Pt: {
    name: 'Platinum',
    atomic_weight: 195.09
  },
  Au: {
    name: 'Gold',
    atomic_weight: 196.97
  },
  Hg: {
    name: 'Mercury',
    atomic_weight: 200.59
  },
  Tl: {
    name: 'Thallium',
    atomic_weight: 204.37
  },
  Pb: {
    name: 'Lead',
    atomic_weight: 207.19
  },
  Bi: {
    name: 'Bismuth',
    atomic_weight: 208.98
  },
  Po: {
    name: 'Polonium',
    atomic_weight: 210
  },
  At: {
    name: 'Astatine',
    atomic_weight: 211
  },
  Rn: {
    name: 'Radon',
    atomic_weight: 222
  },
  Fr: {
    name: 'Francium',
    atomic_weight: 223
  },
  Ra: {
    name: 'Radium',
    atomic_weight: 226.05
  },
  Ac: {
    name: 'Actinium',
    atomic_weight: 227.05
  },
  Th: {
    name: 'Thorium',
    atomic_weight: 232.12
  },
  Pa: {
    name: 'Protactinium',
    atomic_weight: 231.05
  },
  U: {
    name: 'Uranium',
    atomic_weight: 238.07
  },
  Np: {
    name: 'Neptunium',
    atomic_weight: 237
  },
  Pu: {
    name: 'Plutonium',
    atomic_weight: 239
  },
  Am: {
    name: 'Americium',
    atomic_weight: 241
  },
  Cm: {
    name: 'Curium',
    atomic_weight: 242
  },
  Bk: {
    name: 'Berkelium',
    atomic_weight: 243
  },
  Cf: {
    name: 'Californium',
    atomic_weight: 251
  },
  Es: {
    name: 'Einsteinium',
    atomic_weight: 246
  },
  Fm: {
    name: 'Fermium',
    atomic_weight: 250
  },
  Md: {
    name: 'Mendelevium',
    atomic_weight: 256
  },
  No: {
    name: 'Nobelium',
    atomic_weight: 254
  },
  Lr: {
    name: 'Lawrencium',
    atomic_weight: 257
  },
  Rf: {
    name: 'Rutherfordium',
    atomic_weight: 267
  },
  Db: {
    name: 'Dubnium',
    atomic_weight: 268
  },
  Sg: {
    name: 'Seaborgium',
    atomic_weight: 269
  },
  Bh: {
    name: 'Bohrium',
    atomic_weight: 270
  },
  Hs: {
    name: 'Hassium',
    atomic_weight: 277
  },
  Mt: {
    name: 'Meitnerium',
    atomic_weight: 278
  },
  Ds: {
    name: 'Darmstadtium',
    atomic_weight: 281
  },
  Rg: {
    name: 'Roentgenium',
    atomic_weight: 282
  },
  Cn: {
    name: 'Copernicium',
    atomic_weight: 285
  },
  Nh: {
    name: 'Nihonium',
    atomic_weight: 286
  },
  Fl: {
    name: 'Flerovium',
    atomic_weight: 289
  },
  Mc: {
    name: 'Moscovium',
    atomic_weight: 290
  },
  Lv: {
    name: 'Livermorium',
    atomic_weight: 293
  },
  Ts: {
    name: 'Tennessine',
    atomic_weight: 294
  },
  Og: {
    name: 'Oganesson',
    atomic_weight: 294
  }
}