function parseMolecule(formula) {
  var lexemes = [];
  for (var i = 0; i < formula.length; i++) {
    // Separate out individual atoms. Names can have more than one letter, but always start from a capital letter.
    if (formula[i] >= 'A' && formula[i] <= 'Z') {
      for (var j = i + 1; j < formula.length && formula[j] >= 'a' && formula[j] <= 'z'; j++)
        /* pass */;
      lexemes.push(formula.slice(i, j));
      i = j - 1;
    } else if (formula[i] >= '0' && formula[i] <= '9') {
      for (var j = i + 1; j < formula.length && formula[j] >= '0' && formula[j] <= '9'; j++)
        /* pass */;
      lexemes.push(parseInt(formula.slice(i, j)));
      i = j - 1;
    } else {
      throw new Error();
    }
  }

  var result = {};
  for (var j = 0; j < lexemes.length; j++) {
    if (lexemes[j][0] >= 'A' && lexemes[j][0] <= 'Z' && typeof(lexemes[j + 1]) !== 'number') {
      result[lexemes[j]] = 1;
    } else if (lexemes[j][0] >= 'A' && lexemes[j][0] <= 'Z') {
	  result[lexemes[j]] = lexemes[j + 1];
	  j++;
      } else {
      throw new Error();
      }
	}
  return result;
}